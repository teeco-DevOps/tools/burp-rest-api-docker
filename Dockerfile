FROM alpine as burp
RUN apk add wget
WORKDIR /burp-rest-api
RUN  mkdir /burp-rest-api/build/libs -p && wget "https://github.com/vmware/burp-rest-api/releases/download/v2.2.0/burp-rest-api-2.2.0.jar" -O build/libs/burp-rest-api-2.2.0.jar
RUN echo "" > empty
RUN wget "https://portswigger-cdn.net/burp/releases/download?product=pro&version=1.7.34&type=Jar" -O burpsuite_pro_v1.7.34.jar
FROM gcr.io/distroless/java:8
WORKDIR /app
COPY --from=burp /burp-rest-api/build/libs/burp-rest-api-2.2.0.jar /app/
COPY lib /app/lib
COPY --from=burp /burp-rest-api/burpsuite_pro_v1.7.34.jar /app/lib
COPY prefs.xml /root/.java/.userPrefs/burp/prefs.xml
COPY burp-rest-api.sh /app
COPY proxy.json /app/proxy.json
EXPOSE 8080 8090
COPY --from=burp /burp-rest-api/empty /etc/java-8-openjdk/accessibility.properties
ENTRYPOINT ["java", "-Xbootclasspath/p:lib/ESEdition.jar", "-cp", "/app/lib/burpsuite_pro_v1.7.34.jar:/app/burp-rest-api-2.2.0.jar:", "org.springframework.boot.loader.JarLauncher","--address=0.0.0.0","--config-file=proxy.json"]