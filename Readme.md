# Burpsuite Rest API
This is just containerized solution for burp-rest-api, more documentation about this plugin can be found [here](burp-rest-api/).

## Included in the Container
1. prefs.xml ( the license file for BurpSuite professional )
2. proxy.json ( the user configuration file for BurpSuite that has all needed configurations enabled)
3. burpsuite.jar ( the professional version of BurpSuite )
4. burp-rest-api.jar ( the api jar for BurpSuite )
## Usage
```sh
docker run -d -p 8080:8080 -p 8090:8090 registry.gitlab.com/teeco-devops/tools/burp-rest-api-docker:latest
```
1. It exposes port 8080 for BurpSuite's Proxy.
2. It exposes port 8090 for BurpSuite's API.